#ifndef ENEMY_H
#define ENEMY_H

void enemy_spawn_logic_easy(void);
void enemy_spawn_logic_normal(void);
void enemy_spawn_dark_orb(void);
void clear_enemies(void);
int count_enemies(void);
void enemy_logic(void);
void enemy_render(void);
void enemy_render_shadow(void);

#endif
