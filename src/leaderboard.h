#ifndef LEADERBOARD_H
#define LEADERBOARD_H

void download_leaderboard(void);
void leaderboard_logic(void);
void leaderboard_render(void);

#endif
